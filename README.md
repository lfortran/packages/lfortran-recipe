# Conda Recipe for LFortran

## Versioning

Besides creating a conda package, this repository also creates a whole packed
environment using conda-pack, which gets uploaded to dropbox.

The version is taken from the lfortran version, say, 0.1.1, and appends the
build number, say, -5 (this gets incremented every time a change is made to
this repository). So together we get 0.1.1-5.

## Usage

### Binary

```
wget https://www.dropbox.com/s/4ekt89h09qxfifo/lfortran_nb-0.1.5-2-linux-64.bin
chmod +x lfortran_nb-0.1.5-2-linux-64.bin
./lfortran_nb-0.1.5-2-linux-64.bin
```

### Tarball

```
wget https://www.dropbox.com/s/qjfwy0j779wlojg/lfortran_dist-0.1.5-2-linux-64.tar.gz
tar xzf lfortran_dist-0.1.5-2-linux-64.tar.gz
source lfortran_dist/bin/activate
conda-unpack
lfort --help
jupyter notebook lfortran_dist/share/lfortran/nb/Demo.ipynb
```
