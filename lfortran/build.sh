lfortran_version=0.1.5
tar xzf lfortran-${lfortran_version}.tar.gz
cd lfortran-${lfortran_version}
./build1.sh
${PYTHON} -m pip install . --no-deps --ignore-installed -vvv
