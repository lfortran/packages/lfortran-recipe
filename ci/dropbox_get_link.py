"""
Returns a download link to a dropbox file.

Usage in Bash:

    link=$(python dropbox_get_link.py /some/path/to/file/on/dropbox)

Put your dropbox app key into the DROPBOX_DEPLOY_TOKEN environment variable.
"""

import os
import sys

import dropbox

dropbox_key = os.environ["DROPBOX_DEPLOY_TOKEN"]
file_path = sys.argv[1]

d = dropbox.Dropbox(dropbox_key)
l = d.sharing_create_shared_link(file_path)
link = l.url.split("?")[0]
print(link)
