#!/bin/bash

set -e
set -x

if [[ $CI_COMMIT_TAG == "" ]]; then
    # The commit is not tagged, use the first 7 chars of git commit hash
    version=${CI_COMMIT_SHA:0:7}
    dest_dir="test"
else
    # Release version
    # The commit is tagged, convert tag version properly:v1.0.1 -> "1.0.1"
    version=${CI_COMMIT_TAG:1}
    dest_dir="release"
fi
dest_path="/${dest_dir}/lfortran_nb-${version}-linux-64.bin"

wget https://raw.githubusercontent.com/andreafabrizi/Dropbox-Uploader/b362ebf91bdf3619509210ed28b6201d7b189c69/dropbox_uploader.sh

set +x
echo "OAUTH_ACCESS_TOKEN=${DROPBOX_DEPLOY_TOKEN}" > dropbox_uploader.cfg
set -x

bash dropbox_uploader.sh -f dropbox_uploader.cfg upload lfortran.bin ${dest_path}
link=$(python ../ci/dropbox_get_link.py ${dest_path})

echo "Download link:"
echo ${link}
